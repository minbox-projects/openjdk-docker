# openjdk-docker

提供构建OpenJDK二进制文件的Docker镜像脚本

### 构建脚本
```shell
./build.sh 11/jre/Dockerfile-openjdk11-openj9-full linux/amd64,linux/arm64 java/jre:openjdk11-linux-openj9
```

> `docker buildx` 启用详见文档：[https://github.com/docker/buildx](https://github.com/docker/buildx)